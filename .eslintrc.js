module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: ['plugin:vue/essential', 'eslint:recommended', '@vue/prettier'],
    parserOptions: {
        parser: 'babel-eslint',
    },
    rules: {
        'no-console': 0,
        'no-debugger': 1,
        'no-unused-vars': 1,
        indent: [1, 4, { SwitchCase: 1 }],
        'end-of-line': 0,
    },
};
